## Requisitos

### Software stack

- MySQL 5.7+ (preferido) o MariaDB 10.1+ 
- PHP 7.2+ (preferido 7.2.4)
- Laravel 5.6+
- Apache 2.4

### Instalacion del ambiente de desarrollo
+ `composer install`
+ `php -r "file_exists('.env') || copy('.env.example', '.env');"`
+ `php artisan key:generate`
+ `php artisan storage:link`
+ `importar en DB database/database_script.sql`


### Notas extra

En el archivo .env es donde se debera configurar la conexion a la base de datos, asi como el
correo que se planea usar, el tipo de entorno, nombre la aplicacion.
